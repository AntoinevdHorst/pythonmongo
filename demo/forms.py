from django import forms

from demo.models import User


class AddDocumentForm(forms.Form):
    email = forms.CharField(label='Email', max_length=100)
    first_name = forms.CharField(label='first name', max_length=50)
    last_name = forms.CharField(label='last name', max_length=50)
    comment = forms.CharField(label='comment', max_length=50)
    cookie = forms.CharField(label='cookie', max_length=50)


class AddOneTomanyDocumentForm(forms.Form):
    car = forms.CharField(label='car')
    first_name_of_the_first_owner = forms.CharField(label='first name of the first owner')
    first_name_of_the_second_owner = forms.CharField(label='first name of the second owner')


class ChangeDocumentForm(forms.Form):
    CHOICES = []
    i = 0
    for employee in User.objects:
        CHOICES.append((employee.email, employee.email,))
        i += 1
    emails = forms.ChoiceField(choices=CHOICES)
    new_first_name = forms.CharField(label='new first name', max_length=50)


class RemoveDocumentForm(forms.Form):
    email_to_delete = forms.CharField(label='email to delete', max_length=100)
