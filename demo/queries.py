from demo.models import *


def get_all_query(user_first_name):
    queryobject = User.objects(first_name=user_first_name)
    return (queryobject)


def add_new_user(email, first_name, last_name, comments, cookie_name, mongo, sql):
    if mongo:
        add_user = User.objects.create(
            email=email,
            first_name=first_name,
            last_name=last_name,
            comments=[Comment(content=comments)],
        )
        add_user.save()

        add_cookie = Cookie.objects.create(
            cookie_name=cookie_name,
            consumer=add_user,
        )
        add_cookie.save()
    if sql:
        add_user = SqlUser.objects.create(
            email=email,
            first_name=first_name,
            last_name=last_name
        )
        add_user.save()
        add_cookie = SqlCookie.objects.create(
            cookie_name=cookie_name,
            consumer=add_user
        )
        add_cookie.save()

    # more ifs for more databases, etc etc
    return


def remove_user(email, mongo, sql):
    if mongo:
        User.objects(email=email).delete()
    if sql:
        SqlUser.objects.filter(email=email).delete()

    return


def change_user(email, new_first_user, mongo, sql):
    if mongo:
        User.objects(email=email).update_one(set__first_name=new_first_user)
    if sql:
        SqlUser.objects.filter(email=email).update(first_name=new_first_user)

    return


def new_one_to_many_user(owner1, owner2, car_brand, mongo, sql):
    if mongo:
        get_first_user = User.objects(first_name=owner1).first()
        get_second_user = User.objects(first_name=owner2).first()
        Car.objects.create(
            brand=car_brand,
            owner=[get_first_user, get_second_user]).save()
    if sql:
        get_first_user = SqlUser.objects.get(first_name=owner1)
        get_second_user = SqlUser.objects.get(first_name=owner2)
        first_user = SqlCar.objects.create(
            brand=car_brand,
            owner=get_first_user
        )
        first_user.save()

        second_user = SqlCar.objects.create(
            brand=car_brand,
            owner=get_second_user
        )
        second_user.save()
    return


def just_a_test(mongo, sql):
    import collections
    mongoresult = None
    sqlresult = 2
    name_collcetion = collections.namedtuple('result', ['mongoresult', 'sqlresult'])
    aap = name_collcetion(mongoresult, sqlresult)
    return aap
