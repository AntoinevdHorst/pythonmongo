from django.http import HttpResponseRedirect
from django.shortcuts import render

from demo.forms import AddDocumentForm, AddOneTomanyDocumentForm, ChangeDocumentForm, RemoveDocumentForm
from demo.models import User, Cookie, Bulkiebulkie, Car
from demo.queries import add_new_user, remove_user, change_user, new_one_to_many_user

mongo = 1
sql = 1


def index(request):
    class Messenger:
        def __init__(self, **kwargs):
            self.__dict__ = kwargs

    m = Messenger(info="some information", b=['a', 'list'])
    m.more = 11
    m.first_name = "henkie"
    print(m.info, m.b, m.more)
    return render(request, 'demo/index.html')


def add_document(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AddDocumentForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            # add_user = User.objects.create(
            #     email=form.cleaned_data['email'],
            #     first_name=form.cleaned_data['first_name'],
            #     last_name=form.cleaned_data['last_name'],
            #     comments=[Comment(content=form.cleaned_data['comment'])],
            # )
            # add_user.save()
            # add_cookie = Cookie.objects.create(
            #     cookie_name=form.cleaned_data['cookie'],
            #     consumer=add_user,
            # )
            # add_cookie.save()

            add_new_user(form.cleaned_data['email'], form.cleaned_data['first_name'], form.cleaned_data['last_name'],
                         form.cleaned_data['comment'], form.cleaned_data['cookie'], mongo, sql)
            return HttpResponseRedirect('/demo/')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = AddDocumentForm()
    return render(request, 'demo/addDocument.html', {'form': form})


def remove_document(request):
    if request.method == 'POST':
        form = RemoveDocumentForm(request.POST, request.FILES)
        if form.is_valid():
            remove_user(form.cleaned_data['email_to_delete'], mongo, sql)
            return HttpResponseRedirect('/demo/results')
        else:
            return HttpResponseRedirect('/demo')
    else:
        form = RemoveDocumentForm()
    return render(request, 'demo/removeDocument.html', {"form": form})


def change_document(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ChangeDocumentForm(request.POST, request.FILES)
        if form.is_valid():
            # email_to_change = form.cleaned_data['emails']
            # new_name = form.cleaned_data['new_first_name']
            # User.objects(email=email_to_change).update_one(set__first_name=new_name)
            change_user(form.cleaned_data['emails'], form.cleaned_data['new_first_name'], mongo, sql)
            return HttpResponseRedirect('/demo/results')
        else:
            return HttpResponseRedirect('/demo/')
    else:

        form = ChangeDocumentForm()
    return render(request, 'demo/changedocument.html', {"form": form})


def add_one_to_many_document(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AddOneTomanyDocumentForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            new_one_to_many_user(form.cleaned_data["first_name_of_the_first_owner"],
                                 form.cleaned_data["first_name_of_the_second_owner"], form.cleaned_data['car'], mongo,
                                 sql)
            return HttpResponseRedirect('/demo/results')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = AddOneTomanyDocumentForm()
    return render(request, 'demo/addonetomanydocument.html', {'form': form})


def bulkinsert(request):
    if request.GET.get('bulkbtn'):
        test = [Bulkiebulkie(content="string"), ]
        query = test * 100000
        Bulkiebulkie.objects.insert(query)
        return HttpResponseRedirect('/demo/bulkinsert', )
    return render(request, 'demo/bulkinsert.html')


def results(request):
    i = 0
    user_list = []
    for user_object in User.objects:
        user_list.append(user_object.id)
        user_list.append(user_object.email)
        user_list.append(user_object.first_name)
        user_list.append(user_object.last_name)
        user_list.append(user_object.to_json())
        i += 1
    i = 0
    cookie_list = []
    for cookie_object in Cookie.objects:
        cookie_list.append(cookie_object.id)
        cookie_list.append(cookie_object.cookie_name)
        cookie_list.append(cookie_object.consumer.id)
        i += 1

    i = 0
    car_list = []
    for car_object in Car.objects:
        car_list.append(car_object.id)
        car_list.append(car_object.brand)
        car_list.append(Car.objects.only('owner').to_json())
        i += 1
    bulkie_bulkie_count = Bulkiebulkie.objects.count()

    return render(request, 'demo/results.html',
                  {'bulkie_bulkie_count': bulkie_bulkie_count, 'userlist': user_list,
                   'cookielist': cookie_list, 'carlist': car_list, })
