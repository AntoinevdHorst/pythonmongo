import datetime

import mongoengine
from django.db import models
from mongoengine import *


# Demoapp models
class Comment(EmbeddedDocument):
    content = StringField()


class User(Document):
    comments = ListField(EmbeddedDocumentField(Comment))
    email = StringField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    meta = {
        'indexes': [
            'email',
            '$email',  # text index
            '#email',  # hashed index
        ]
    }


class Cookie(Document):
    cookie_name = StringField()
    consumer = ReferenceField(User, reverse_delete_rule=mongoengine.CASCADE)


class Car(Document):
    brand = StringField()
    owner = ListField(ReferenceField(User, reverse_delete_rule=mongoengine.CASCADE))


class Bulkiebulkie(Document):
    content = StringField()


# sql models


class SqlUser(models.Model):
    email = models.CharField(max_length=200)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=201)


class SqlCookie(models.Model):
    cookie_name = models.CharField(max_length=210)
    consumer = models.ForeignKey(
        'demo.SqlUser',
        on_delete=models.CASCADE,
    )


class SqlCar(models.Model):
    brand = models.CharField(max_length=200)
    owner = models.ForeignKey(
        'demo.SqlUser',
        on_delete=models.CASCADE,
    )


# HDP models
class Researcher(EmbeddedDocument):
    researcher_ID = IntField()
    last_name = StringField()
    first_name = StringField()


class Participant(EmbeddedDocument):
    person_id = ReferenceField("Person")
    # personid?


class Weight(EmbeddedDocument):  # datetime value# datetime value
    placeholder = StringField()


class Xxxxx(EmbeddedDocument):  # datetime value# datetime value
    placeholder = StringField()


class Length(EmbeddedDocument):  # datetime value# datetime value
    placeholder = StringField()


class Characteristics(EmbeddedDocument):
    length = ListField(EmbeddedDocumentField(Length))  # over 16 mb?
    weight = ListField(EmbeddedDocumentField(Weight))
    xxxxxx = ListField(EmbeddedDocumentField(Xxxxx))  # wat is dit?


class Research(Document):
    research_id = IntField()
    description = StringField()
    researchers = ListField(EmbeddedDocumentField(Researcher))
    start_date = DateTimeField()
    end_date = DateTimeField()
    participants = ListField(EmbeddedDocumentField(Participant))


class Participations(EmbeddedDocument):
    research_group = StringField()
    description = StringField()
    start_date = DateTimeField()
    end_date = DateTimeField()


class MeasurementPerDay(Document):
    datetime = DateTimeField(default=datetime.datetime.now)
    person_id = ReferenceField("Person")  # circlereference. maybe dirty?
    research_id = ListField(ReferenceField(Research))  # neccesary?
    wearable_id = IntField()  # neccesary?
    # number of steps, ?


class Wearable(EmbeddedDocument):
    wearable_id = ListField(ReferenceField(
        MeasurementPerDay))  # weird?? should be other way around? reverse cascade doesnt work for embedded stuff
    type = StringField()
    description = StringField()


class Person(Document):
    person_id = IntField()
    # cant reference to an embedded doc
    last_name = StringField()
    first_name = StringField()
    email = StringField()
    date_of_birth = DateTimeField()
    gender = StringField()
    characteristics = ListField(EmbeddedDocumentField(Characteristics))
    participation = ListField(EmbeddedDocumentField(Participations))
    wearable = ListField(EmbeddedDocumentField(Wearable))


class Intervention(Document):
    person_id = ReferenceField(Person, reverse_delete_rule=mongoengine.CASCADE)
    datetime = DateTimeField(default=datetime.datetime.now)
    intervention_type = StringField()
    intervention_description = StringField()


class Prediction(Document):
    person_id = ReferenceField(Person, reverse_delete_rule=mongoengine.CASCADE)
    datetime = DateTimeField(default=datetime.datetime.now)
    prediction_type = StringField()
    predicted_value = IntField()
    probability = StringField()
    model = StringField()


class Test(EmbeddedDocument):
    type = StringField()
    description = StringField()
    datetime = DateTimeField(default=datetime.datetime.now)
    value = StringField()  # correct?


class Diary(EmbeddedDocument):
    event = StringField()
    datetime = DateTimeField()
    description = StringField()
    question = StringField()
    answer = StringField()


class AdditionalInfo(Document):
    person_id = ReferenceField(Person, reverse_delete_rule=mongoengine.CASCADE)
    datetime = DateTimeField(default=datetime.datetime.now)
    description = StringField()
    test = ListField(EmbeddedDocumentField(Test))
    diary = ListField(EmbeddedDocumentField(Diary))
